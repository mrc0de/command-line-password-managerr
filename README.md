## Command Line Password Manager
> A Password Manager that doesn't require internet. Run it directly from your hard-drive, or external hard-drive to ensure that there are **not** prying eyes trying to get all of your passwords. The internet today is a **dangerous place**, and was never (**and still is not**) a safe place to be storing your passwords for your banks, and other sensitive online accounts .

### Installation:
1. **Extract** source code to directory of your choosing.
2. **Execute** the follow statement in your terminal: `python store.py`
3. **Enjoy!**

#### Primary Benefits:
- Store as **many passwords as needed**, unlike existing *online solutions* that limit you.
- Use of **military grade** encryption standards, utilizing proven crypto standards for password storage.

#### Features:
- **Password-Protection** of database containing all encrypted information 
- **Copy** your *username* or *password* for a given account to your system memory for easy access online.
- **Generates** *cryptographically secure* passwords to properly change your passwords as needed, on your own terms... set if you need: *digits*, *special characters*, and the *number of characters* for a given password.

Questions? Just ask, and most importantly enjoy!
