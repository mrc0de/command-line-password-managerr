import sqlite3
import os
import time
import shutil
import string
import random
import getpass
from datetime import datetime

from tabulate import tabulate
from simplecrypt import encrypt, decrypt
import pprint; pp = pprint.PrettyPrinter(indent=2)
import clipboard

#ON MAC/LINUX
#import os;os.chdir("path-root-dir");os.chdir("path-current-dir");import store

#ON WINDOWS
#import os; os.chdir(r"path-root-dir"); import store

def create_and_setup():
    '''
    create_and_setup() performs initial setup duties of the database, and structure.
    It returns a connection to a sqlite3 database 'pw.db'.  
    '''
    #if pw.db DOES NOT EXIST
    if not os.path.isfile('pw.db'): 
        conn = sqlite3.connect('pw.db')
        connection = conn.cursor()
        print("\npw.db created !") 
        try:
            connection.execute('''
                CREATE TABLE category (
                cat_id              INTEGER PRIMARY KEY AUTOINCREMENT, 
                cat_name            TEXT NOT NULL
                );
            ''')
            print('Category Table Created!') 
        except Exception as e:
            print('Error {} creating {} table'.format(e, 'category'))
            
        try:
            connection.execute('''
                CREATE TABLE my_acct (
                acct_id             INTEGER  PRIMARY KEY AUTOINCREMENT,
                acct_url            TEXT NOT NULL,
                acct_name           TEXT NOT NULL,
                acct_username       TEXT NOT NULL,
                acct_salt           TEXT NOT NULL,
                acct_password       TEXT NOT NULL,
                acct_extra_info     TEXT NULL,
                created_date        NUMERIC NOT NULL,
                category_id         INTEGER, 
                FOREIGN KEY(category_id) REFERENCES category(cat_id)
                );          
            ''')
            print('my_acct Table Created!\n')
        except Exception as e:
            print('Error {} creating {} table\n'.format(e, 'my_acct'))

        #TODO: create table to store master pass.

        return conn, connection

    elif os.path.isfile('pw.db'):
        conn = sqlite3.connect('pw.db')
        connection = conn.cursor()

    return conn, connection

def set_categories(conn, connection):
    '''
    set_categories sets all of the categories used to associate an account to a category

    It continually asks for categories until '-' is entered.

    Once all categories are set, and - is entered, the function will import into category table.
    '''

    categories = []
    decision_to_stop = False
    while decision_to_stop != True:
        prompt_category_name = input('What is the category C? (press - to quit)\n')
        if prompt_category_name == '-':
            decision_to_stop = True
        elif not prompt_category_name:
            decision_to_stop = False
        else:
            categories.append(prompt_category_name)
    for cat in categories:
        try:
            conn.execute("""
            INSERT INTO category
            (cat_name)
            VALUES (?) """, (cat,)
            )       
            conn.commit()
            print('inserted: {}'.format(cat))

        except Exception as e:
            print('Error Occurred inserting category: {} \n {}'.format(cat, e))  

def get_categories(conn):
    '''
    get_categories() retrieves all of the categories entered.
    '''
    try:
        gather_categories = conn.execute("""
        SELECT * FROM category;"""
        )

    except Exception as e:
        print('Error Occurred : {}'.format(e)) 

    return gather_categories

def set_account(conn, connection, gather_categories):
    '''
    set_account() takes the connection from create_and_setup, it then performs:
    - Collecting 1, or multiple accounts, and importing them into the my_acct db table.
    
    Fields in database inserted into: acct_name | acct_username | acct_password
    
    '''
    account = {}
    decision_enter_account = input('Want to enter your account(s)? (Y for Yes) \n')
    
    if decision_enter_account == 'Y':
        bool_add_account = True
        while bool_add_account == True:
            #prompt for initial values for account_name, account_username, account_password
            prompt_account_url  = input('What is the account\'s url?\n')
            prompt_account_name = input('\nWhat is the account name? \n')
            prompt_account_username = input('What is {}\'s username? \n'.format(prompt_account_name))
            prompt_account_password = getpass.getpass(prompt='What is {}\'s password? \n'.format(prompt_account_name))  
            print('Your categories:\n')
            for cat in gather_categories:
                print(cat)   
            prompt_account_category = int(
                                        input('Which category should we assign {} to? Please enter the number. (e.g. 1)\n'.
                                        format(prompt_account_name)
                                        )
            )
            prompt_account_extra_info = input('If {} has extra info, please enter it now. (or enter - for No)\n'.format
                                            (prompt_account_name)
            )
            if prompt_account_extra_info == '-':
                prompt_account_extra_info = 'NULL'
            
            print('\nEncrypting account details...\n')

            #check for empty values:
            if not prompt_account_name:
                prompt_account_name = input('What is the account name? \n')
            if not prompt_account_username:
                prompt_account_username = input('What is {}\'s username? \n'.format(prompt_account_name))
            if not prompt_account_password:
                prompt_account_password = input('What is {}\'s password? \n'.format(prompt_account_name))
            if not prompt_account_category:
                for cat in gather_categories:
                    print(cat)   
                prompt_account_category = int(input('Which category would you like to assign this account to? Please enter the number.\n'))

            salt = os.urandom(8)
            str(salt)
            hashed_username = encrypt(salt, prompt_account_username)
            hashed_pass = encrypt(salt, prompt_account_password)
            extra_info = prompt_account_extra_info

            account[prompt_account_name] = [
                                            hashed_username, 
                                            hashed_pass, 
                                            salt, 
                                            prompt_account_category, 
                                            prompt_account_url,
                                            extra_info
            ]           

            #print("Done!\n")
            #control flow
            decision_add_accounts = input('Do you have more accounts to add? (Y for Yes, N for No)\n')            
            if decision_add_accounts == 'Y':
                bool_add_account = True
            elif decision_add_accounts == 'N':
                bool_add_account = False
            else:
                print('You didn\'t enter in a valid value of {} for Yes or {} for No '.format('Y', 'N'))
            
            for k,v in account.items():
                username_insert = v[0]
                password_insert = v[1]
                salt_insert     = v[2]
                category_insert = v[3]
                url             = v[4]
                extra           = v[5]

                try:
                    conn.execute("""
                        INSERT INTO my_acct
                                        (acct_url, 
                                         acct_name, 
                                         acct_username, 
                                         acct_password, 
                                         acct_extra_info,
                                         acct_salt, 
                                         created_date, 
                                         category_id
                                        )
                        VALUES (?,?,?,?,?,?,?,?) """, 
                        (
                            url,
                            k, 
                            username_insert, 
                            password_insert, 
                            extra,
                            salt_insert, 
                            datetime.now(),
                            category_insert, 
                        )
                    )
                    conn.commit()    
                    print('inserted account: {}\n'.format(k)) 
                except Exception as e:
                   print('Error Occurred inserting: {}'.format(e))
    
    return account

def get_accounts(conn):
    '''
    get_accounts() gets all of the accounts available in the database.
    '''
    try:
        gather_accounts = conn.execute("""
            SELECT acct_url,acct_name, acct_username, acct_password, category_id 
            FROM my_acct;
        """)

    except Exception as e:
        print('Error Occurred : {}'.format(e)) 

    return gather_accounts

def generate_password():
    '''
    generate_password() does exactly that will generate strong, and secure passwords based
    on your needs. 
    * It prompts a user for the length of the password.
    It currently allows:
    - Choice to Disallow special characters, and numbers as needed.
    '''
    length_of_password = int(input('\nHow long can your password be? e.g. 20\n'))
    numbers_allowed = input('Are numbers allowed? (Y for Yes, N for No)')
    special_allowed = input('Are special characters allowed? (Y for Yes, N for No)')
    disallowed_char = input('Disallowed chars? e.g. ;,#,! \n')
    disallowed_char = list(disallowed_char)
    disallowed_char.append('`')
    disallowed_char.append('\'')
    disallowed_char.append('\"')
    print('Disallowed chars: {}'.format(disallowed_char))

    chars = string.ascii_lowercase + string.ascii_uppercase

    if numbers_allowed == 'Y':
        chars = chars + string.digits
    
    if special_allowed == 'Y':
        chars = chars + string.punctuation

    generated_password = ''.join((random.choice(chars)) for x in range(length_of_password))

    for dc in disallowed_char:
        if dc in generated_password:
            generated_password = generated_password.replace(dc, random.choice(chars))

    return generated_password
    
def update_account(conn, connection, gather_accounts, generated_password):
    '''
    update_account() does exactly as it states, it updates something about an account.
    This currently includes:
        * acct_password
    '''
    prompt_account_to_update = input('What account would you like to update? Type none for no account\n')
    ##
    #Take in the user input of account to update:
    for account in gather_accounts:
        if account[1] == prompt_account_to_update:
            print('\nOkay, you want to update account: {}\n'.format(prompt_account_to_update))
            decision_update_password = input('\nDo you want to update account: {}\'s password? (Y for Yes N for No)'
                                        .format(prompt_account_to_update)
            )
            if decision_update_password == 'Y':
                generate_password()
                salt = os.urandom(8)
                str(salt)
                new_pass = encrypt(salt, generated_password)
                acct_id = account[0]
                print('salt to be inserted: {} \nnew_pass to be inserted: {} \nacct_id to be inserted: {} \n'.format(
                    salt,
                    new_pass,
                    acct_id
                    ))
                print('type(salt) {} \ntype(new_pass): {} \nntype(acct_id){} \n'.format(
                    type(salt),
                    type(new_pass),
                    type(acct_id)
                    ))
                try:
                    conn.execute("""
                        UPDATE my_acct
                        SET acct_password = ?
                        WHERE acct_id = ?
                        """, 
                        (
                        new_pass, 
                        acct_id
                        )
                    )
                    conn.commit()
                    print('Updated account {} new pass was:\n{}'.format(prompt_account_to_update, generated_password))
                except Exception as e:
                    print('Error Occurred inserting : {}'.format(e))
            elif decision_update_password == 'N':
                print('We won\'t update password for account: {}'.format(prompt_account_to_update))
            else:
                print('\nYou must enter \'Y\' or \'N\' \n')

def remove_account(conn, connection, gather_accounts):
    '''
    remove_account() - removes an account from sqlitedb
    '''
    
    gather_accounts = list(gather_accounts)
    
    for account in gather_accounts:
        print('Account Name: {}\n'.format(account[1]))

    valid_accounts = [account[1] for account in gather_accounts]
    #print('valid_accounts: {}\n'.format(valid_accounts))

    account_to_delete = input('What account would you like to DELETE?\n')  
    try:
        if account_to_delete not in valid_accounts:
            print('Sorry account_to_delete: {} is not in the db.'.format(account_to_delete))
        else:
            conn.execute("DELETE FROM my_acct WHERE acct_name=?", (account_to_delete,))
            conn.commit()
            print('deleted: {}'.format(account_to_delete))
    except Exception as e:
        print('Error Occurred deleting account: {} \n {}'.format(account_to_delete, e))  
    
def get_account_and_categories(conn):
    '''
    get_account_and_categories() defines a function to gathers a users accounts, and their corresponding categories.
    '''
    try:
        gather_account_and_categories = conn.execute("""
            SELECT acct_name, cat_name 
            FROM my_acct, category 
            WHERE my_acct.category_id = category.cat_id 
            ORDER BY my_acct.acct_name, my_acct.category_id ASC
        """ )
        print('{} {} {}'.format('\nAccount Name','       ', 'Category'))
        list_gather_account_and_categories = list(gather_account_and_categories)
        print(tabulate(list_gather_account_and_categories))
        
    except Exception as e:
        print('Error Occurred : {}'.format(e)) 

    for ac in gather_account_and_categories:
        print(ac)

    return gather_account_and_categories

def get_single_account_info(conn, gather_accounts):
    '''
    get_single_account_info() will retrieve a single accounts information. It will attempt to decrypt the information.
    '''
    print('Your Accounts:\n')
    for account in gather_accounts:
        print('************************************')
        print('Account Url: {}'.format(account[0]))
        print('Account Name: {}'.format(account[1]))

    prompt_get_single_account = input('\nWhat account would you like info for? (e.g. PNC Bank, or - to cancel)\n')
    while prompt_get_single_account != '-':
        try:
            gather_an_account = conn.execute("""
                SELECT acct_url,acct_name, acct_username, acct_password, acct_salt, cat_name 
                FROM my_acct, category 
                WHERE my_acct.acct_name = ?
            """, (prompt_get_single_account,))
            single_account = gather_an_account.fetchone()
            list(single_account)
            param_url              = single_account[0]
            param_acct_name        = single_account[1]
            param_encoded_username = single_account[2]
            param_encoded_password = single_account[3]
            param_encoded_salt =     single_account[4]
            param_encoded_category = single_account[5]

            print('\nparam_encoded_category: {}\n'.format(param_encoded_salt))
            print('\nparam_encoded_category: {}\n'.format(param_encoded_category))
            
            #decrypt_db()
            print('************************************\n Account Url: {}'.format(single_account[0]))
            print(' Account Name: {}'.format(single_account[1]))

            #TODO: Get the right category to show
            print(' Account Category: {}\n************************************\n'.format(param_encoded_category))
            print(' Decrypting username and password...\n')
            
            decoded_username = decrypt(
                                param_encoded_salt,
                                param_encoded_username
                            ).decode('utf-8')
                            
            decoded_pass = decrypt(
                                param_encoded_salt,
                                param_encoded_password
                            ).decode('utf-8')

            decision_copy = True
            while decision_copy != False:
                decision_account_detail_to_copy = input('\nCopy: {} or {} or {}? (- to stop)\n'.format(
                                                    'url',
                                                    'username',
                                                    'password'
                                                  )
                )

                if decision_account_detail_to_copy == 'url':
                    clipboard.copy(param_url)
                    print('\nUrl copied to clipboard!\n')
                elif decision_account_detail_to_copy == 'username':
                    clipboard.copy(decoded_username)
                    print('\nUsername copied to clipboard!\n')
                elif decision_account_detail_to_copy == 'password':
                    clipboard.copy(decoded_pass)
                    print('\nPassword copied to clipboard!\n')
                elif decision_account_detail_to_copy == '-':
                    decision_copy = False
                else:
                    print('\nValid options are: {} or {} or {}\n'.format(
                                                    'url',
                                                    'username',
                                                    'password'
                                                  )
                    )
        except Exception as e:
            print('Error occurred while getting account the error was: {}'.format(e))
    else:
        print('Stopping..\n')
    
def encrypt_db():
    '''
    encrypt_db() is only used at the end of working with the sqlite3 database.
    It does just that, it takes a plain-text sqlite3 database file (pw.db) and
    encrypts the contents.
    
    It calls a single method: encrypt_file() with the following params:
        encrypt_file(
            in_filename,
            out_filename,
            chunk_size,
            key,
            iv
        )
    '''
    key = os.urandom(32)
    iv = os.urandom(16)

def decrypt_db():
    '''
    decrypt_db() is only used when wanting to view an account from the sqlite3 database.
    It does just that, it takes a cipher-text sqlite3 database file (enc_pw_db.db) and
    decrypts the contents.
    
    It calls a single method: decrypt_db() with the following params:
        decrypt_file(
            in_filename,
            out_filename,
            chunk_size,
            key,
            iv
        )
    '''
    
    ## TODO: gather key, iv from table.
    
    print('\nDecrypting database...\n')
    from file import decrypt_file
    try:
        decrypt_file('enc_pw_db.db', 'pw_db.db', 8192, key, iv)
        print('\nDecrypting enc_pw_db.db...\nRemoving Encrypted File...')
        os.remove('enc_pw_db.db')
    except Exception as e:
        print('Error decrypting database file because: {}'.format(e))
    
    print('\nDatabase encrypted!')

def control_flow(conn, connection):
    list_function_flow = [
    'Update Account',
    'Delete Account',
    'Get Single Account',
    'Get Accounts with Categories'
    ]
    print('\n/\/\/\/\/\/\/\ Account Functions \/\/\/\/\/\/\/\n')
    for function in list_function_flow:
        print(function)
    print('\n/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n')
    
    decision_function_to_perform = input('\n\nWhat would you like to do? (Press - to stop)\n')

    #requirement(s) below are used by most, if not all functions, so include them.
    conn, connection = create_and_setup()

    while decision_function_to_perform != '-':
        if decision_function_to_perform not in list_function_flow:
            print('That was an invalid choice. Valid Choices are: {}'.format(list_function_flow))
        elif decision_function_to_perform == 'Update Account':
            #gather_accounts = get_accounts(conn)
            generated_password = generate_password()
            gather_accounts = get_accounts(conn)
            update_account(conn, connection, gather_accounts, generated_password)
        elif decision_function_to_perform == 'Delete Account':
            gather_accounts = get_accounts(conn)
            remove_account(conn, connection, gather_accounts)
        elif decision_function_to_perform == 'Get Single Account':
            gather_accounts = get_accounts(conn)
            get_single_account_info(conn,gather_accounts)
        elif decision_function_to_perform == 'Get Accounts with Categories':
            get_account_and_categories(conn)
        
        else:
            print('You didn\'t enter in a valid choice!')#: {}'.format(print function for function in list_function_flow))
    print('\nexiting...\n')

def main():
    conn, connection = create_and_setup()
    decision_set_accounts_and_categories = input('\nWant to set your {} and {} (Y for Yes, N for No)?\n'.
                                           format('Categories', 'Accounts')
    )
    if decision_set_accounts_and_categories == 'Y':
        categories = set_categories(conn, connection)
        gather_categories = get_categories(conn)
        account_creation = set_account(conn, connection, gather_categories)
        print('\nEnding running main() pieces, continuing to control_program now...\n')
    else:
        control_program = control_flow(conn, connection)    

    control_program = control_flow(conn, connection)

main()
